from numpy import sqrt


def err_prop(x, dx, y, dy, operator):
    if operator == "+" or operator == "-":
        dz = sqrt(dx ** 2 + dy ** 2)
    elif operator == "*":
        dz = abs(x * y) * sqrt((dx / x) ** 2 + (dy / y) ** 2)
    elif operator == "/":
        dz = abs(x / y) * sqrt((dx / x) ** 2 + (dy / y) ** 2)
    return dz


def resolution_error(Delta_x):
    return (Delta_x / sqrt(12))


def error_mean(dx, N):
    return dx / N
