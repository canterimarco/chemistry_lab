# -*- coding: utf-8 -*-

'''
Third experience in the lab of chemistry.
The objective was the titration of the oxalic acid, by measuring the pH of the
solution in function of the volume of NaOH added.
'''

from numpy import array, diff, hstack, ones, empty
from matplotlib.pyplot import figure, show, legend, xlabel, ylabel
import matplotlib.pyplot as plt
from errorlib import *


def derive(f, x, df, dx):
    diff_f = diff(f)
    diff_x = diff(x)
    derivative = diff_f / diff_x
    Dderivate = empty(len(derivative))
    for i in range(1, len(f)):
        ddiff_f = err_prop(f[i - 1], df[i - 1], f[i], df[i], "+")
        ddiff_x = err_prop(x[i - 1], dx[i - 1], x[i], dx[i], "+")
        Dderivate[i - 1] = err_prop(diff_f[i - 1], ddiff_f, diff_x[i - 1],
                                    ddiff_x, "/")
    return derivative, Dderivate

# Data
V0_H2C2O4 = 50
MOL_NaOH = 0.1
OFFSET_1 = 0.4
OFFSET_2 = 7.1

vol_1 = array([0.2, 0.9, 1.45, 2.1, 2.6, 3.1, 3.6, 4.1, 4.6, 5.1, 5.3, 5.5,
              5.7, 5.9, 6.1, 6.6, 7.1]) - OFFSET_1
vol_2 = array([7.1, 8.1, 9.1, 9.8, 10, 10.2, 11.2, 11.7, 12, 12.1, 12.2, 12.3,
              12.5, 12.7, 13, 13.2]) - OFFSET_2
pH_1 = array([2.248, 2.331, 2.463, 2.697, 2.938, 3.341, 3.708, 4.006, 4.383,
             4.939, 5.335, 8.563, 10.075, 10.598, 10.869, 11.260, 11.482])
pH_2 = array([2.277, 2.486, 2.846, 3.339, 3.494, 3.655, 4.340, 4.829, 5.397,
             6.065, 8.274, 9.264, 10.044, 10.519, 10.948, 11.108])

# Uncertainty and derivate
dvol_1 = ones(len(vol_1))
dvol_2 = empty(len(vol_2))
dvol_1[0] = 0.001 * 0.3  ######################
dvol_2[0] = 0.001 * 0.3  #######################
for i in range(1, dvol_1.size):
    # dvol_1[i] = (dvol_1[i - 1] ** 2 + dvol_1[0] ** 2) ** 0.5
    dvol_1[i] = err_prop(0, dvol_1[i - 1], 0, dvol_1[0], "+")
for i in range(1, dvol_2.size):
    dvol_2[i] = err_prop(None, dvol_2[i - 1], None, dvol_2[0], "+")
dpH_1 = pH_1 / 100
dpH_2 = pH_2 / 100
derivate_1, dderivate_1 = derive(pH_1, vol_1, dpH_1, dvol_1)
derivate_2, dderivate_2 = derive(pH_2, vol_2, dpH_2, dvol_2)


# Titolazione
all_derivate = hstack([derivate_1, derivate_2])
all_vol = hstack([vol_1[1:len(vol_1)],
                 vol_2[1:len(vol_2)]])
eq_point = all_vol[all_derivate == max(all_derivate)]
n_NaOH = MOL_NaOH * eq_point * 10 ** -3
n_H2C2O4 = n_NaOH / 2
mol_H2C2O4 = n_H2C2O4 / (V0_H2C2O4 * 10 ** -3)

print "The molarity of the oxalic acid is", float(mol_H2C2O4), "M"


# Plots
figure("pH in function of the volume of the base added")
plt.errorbar(vol_1, pH_1, yerr=dpH_1, fmt=',', color="b")
plt.errorbar(vol_2, pH_2, yerr=dpH_2, fmt=',', color="r")
plt.grid(True)
xlabel("Volume of NaOH [mL]")
ylabel("pH")
legend(["First measurament", "Second measurament"], loc="upper left",numpoints=1,markerscale=5)
plt.savefig('misure.png')



figure("Derivative of pH in function of the volume of the base added")
plt.errorbar(vol_1[1:len(vol_1)], derivate_1, yerr=dderivate_1, fmt=',',
             color="b")
plt.errorbar(vol_2[1:len(vol_2)], derivate_2, yerr=dderivate_2, fmt=',',
             color="r")
plt.grid(True)
xlabel("Volume of NaOH [mL]")
ylabel("Derivative of pH")
legend(["First measurament", "Second measurament"], loc="upper left",numpoints=1,markerscale=1000)
plt.savefig('derivate.png')
#show()
